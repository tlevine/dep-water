from django import forms

class ScraperForm(forms.Form):
    dep_user_id = forms.CharField()
    dep_password = forms.CharField(widget = forms.PasswordInput())
