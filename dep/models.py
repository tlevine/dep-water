from django.db import models
from django.contrib.auth.models import User

class DEPUser(models.Model):
    "A user Id for the DEP site"
#   user = models.OneToOneField(User)
    dep_user_id = models.CharField()
    account_number = models.CharField() #This should be the primary key

class Reading(models.Model):
    "A water meter reading"
    date = models.DateTimeField()
    dep_user= models.ForeignKey(DEPUser)
    reading = models.IntegerField()
