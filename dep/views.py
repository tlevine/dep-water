from django.shortcuts import render_to_response
from django.template import RequestContext
from models import Reading, DEPUser
from forms import ScraperForm

def home(request):
    #Reading.objects for a particular DEPUser
    return render_to_response('home.html', {})

def history(request):
    if request.method == "GET":
        #Display a form
        form = ScraperForm()
        return render_to_response(
            'history-new.html',
            RequestContext(request, {"form": form})
        )

    elif request.method == "POST":
        #Start the scrape

        #If the credentials are valid, add or create the user and say so.
        
        return render_to_response('history-running.html', {})

        #If not, load history-new.html
        #
